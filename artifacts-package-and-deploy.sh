#!/usr/bin/env bash
#cat $GOOGLE_APPLICATION_CREDENTIALS | helm registry login -u "_json_key" --password-stdin https://europe-central2-docker.pkg.dev
cat ~/artifact_key.json | helm registry login -u "_json_key" --password-stdin https://europe-central2-docker.pkg.dev

echo -n "Enter chart version in the x.y.z format and press [ENTER]: "
read chart_version
echo "New Chart version is: $chart_version"
sed -i "" "s/0\.0\.0/$chart_version/" Chart.yaml
helm package ../book-service

helm push "book-service-${chart_version}.tgz" oci://europe-central2-docker.pkg.dev/coe-devops-cloud-sandbox/charts
